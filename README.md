# GA502DU-config
What's needed to make Asus Zephyrus G - GA502DU usable on Linux  

**Current distribution**:  
Archlinux on Linux 5.5  

**Working**:  
GPU - NVIDIA 1660Ti Max-Q using nvidia-440  
CPU - AMD Ryzen 7 3750H using opensource driver  
WiFi - Realtek RTL8821CE after compilating the driver  
Brightness control - only with software  
**Broken**:  
BIOS - version 208 and 300 lock the CPU at 800Mhz on linux, stay on 207  
Cooling - supported but not optimal, will work on linux 5.6  
Function keys - EVERYTHING except volume  
Keyboard backlit - workaround is booting on Windows then rebooting on linux  

I use PRIME in order to switch between iGPU and dGPU when I need it

# `NVIDIA`

Install nvidia proprietary driver  

    # pacman -S nvidia
    
and copy `99-amd-nvidia-prime-offload.conf` to `/etc/X11/xorg.conf.d/`  
then blacklist nouveau:

    # vim /etc/modprobe.d/blacklist.conf
    ----------
    blacklist nouveau

finally, add an alias for PRIME: 

    # vim ~/.bashrc OR vim ~/.zshrc
    ----------  
    alias nvr="__NV_PRIME_RENDER_OFFLOAD=1 __GLX_VENDOR_LIBRARY_NAME=nvidia __GL_THREADED_OPTIMIZATION=1"
    alias vkr="__NV_PRIME_RENDER_OFFLOAD=1"

now you can run your software using dGPU with `nvr`

# `AMD`

Install AMD opensource driver and vulkan

    # pacman -S mesa lib32-mesa xf86-video-amdgpu vulkan-radeon lib32-vulkan-radeon 

# `BIOS`

If you're on 208 or 300 version of the BIOS you need to downgrade:  
Copy `GU502DU-AS.207` to a USB key
reboot the computer and press F2 to enter the BIOS  
start EZFlash  
flash `GU502DU-AS.207`

# `WiFi`

Download RTL8821CE driver

    $ git clone https://github.com/tomaspinho/rtl8821ce
    $ cd rtl8821ce
    $ sudo m-a prepare
    $ make
    $ sudo make install
    
Reboot, WiFi should work

# `Links` 

This helped me to get things done

https://forum.manjaro.org/t/asus-rog-zephyrus-ga502du-installation-and-configuration-guide-updated-information-2-29-2020/118392  
https://ubuntuforums.org/showthread.php?t=2420199&page=2


# `MEMO`
It's my personal part when I put things slighty or simply not related to the computer 

**imwheel**  
Increase mouse scrolling speed of my Logitech G502

    # pacman -S imwheel
    $ vim ~/.imwheelrc
    ----------
    ".*"
    None, Up, Button4, 3
    None, Down, Button5, 3

Autostart with GNOME

    $ vim ~/.config/autostart/imwheel.desktop
    ----------
    [Desktop Entry]
    Type=Application
    Exec=imwheel
    Hidden=false
    X-GNOME-Autostart-enabled=true
    Name=imwheel
    Comment=Mouse scrolling control


**Steam**  
Tools: 

    $ yay -S mangohud lib32-mangohud gamemode gnome-shell-extension-gamemode-git  
    $ vim .config/MangoHud/MangoHud.conf  
    ----------
    cpu_temp
    gpu_temp
    font_size=19
    position=top-right
    toggle_hud=F11

Launch command :  
    
    __NV_PRIME_RENDER_OFFLOAD=1
    __GLX_VENDOR_LIBRARY_NAME=nvidia
    __GL_THREADED_OPTIMIZATION=1
    mangohud
    gamemoderun
    %command%

**GE install**  
Custom Proton build  
    
    $ mkdir ~/.steam/root/compatibilitytools.d
    $ wget $(curl -s https://api.github.com/repos/GloriousEggroll/proton-ge-custom/releases/latest | jq -r ".assets[] | select(.name | test(\"${spruce_type}\")) | .browser_download_url")
    $ tar -xf Proton-*
    $ rm Proton-*.tar.gz && mv Proton-* ~/.steam/root/compatibilitytools.d


**Borderlands 3**  
The game need Media Fundation to play WMV files    

    # pacman -S wine python2
    $ git clone https://github.com/z0z0z/mf-install  
    $ git clone https://github.com/z0z0z/mf-installcab
    $ rm -rf "/path/to/steam/compatdata/1085660/"
    $ WINEPREFIX="/path/to/steam/compatdata/1085660/pfx" WINEARCH=win64 wine wineboot
    $ WINEPREFIX="/path/to/steam/compatdata/1085660/pfx" /path/to/mf-install/mf-install.sh
    $ WINEPREFIX="/path/to/steam/compatdata/1085660/pfx" /path/to/mf-installcab/install-mf-64.sh

**Spotify**  
Adblock for Spotify

    # pacman -S xdotool
    $ git clone https://github.com/abba23/spotify-adblock-linux
    $ cd spotify-adblock-linux
    $ make
    # make install
    LD_PRELOAD=/usr/local/lib/spotify-adblock.so spotify & ; disown

**MAKEPKG**  
Boost makepkg compilation by using all cores

    # vim /etc/makepkg.conf
    ----------
    MAKEOPTS='-j8'
    MAKEFLAGS='-j8'
